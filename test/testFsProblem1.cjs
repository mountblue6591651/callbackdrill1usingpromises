const fsProblem1 = require("../fs-problem1.cjs");
const path = require("path");

// Set the absolute path for the random directory
let absolutePathOfRandomDirectory = path.join(__dirname, "RandomJSONs");

// Define the number of random files to be created
let randomNumberOfFiles = 5;

// Step 1: Create a folder asynchronously
fsProblem1
  .createFolder(absolutePathOfRandomDirectory)
  .then((msg) => {
    // Log success message when the folder is created
    console.log(msg);
  })
  .then(() => {
    // Step 2: Create files asynchronously after the folder is created
    return fsProblem1.createFiles(absolutePathOfRandomDirectory, randomNumberOfFiles);
  })
  .then((msg) => {
    // Log success message when all files are created
    console.log(msg);
    // Step 3: Delete files asynchronously after they are created
    return fsProblem1.deleteFiles(absolutePathOfRandomDirectory, randomNumberOfFiles);
  })
  .then((msg) => {
    // Log success message when all files are deleted
    console.log(msg);
  })
  .catch((error) => {
    // Log any errors that occur during the process
    console.error(error);
  });
