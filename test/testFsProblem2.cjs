const fsProblem2 = require("../fs-problem2.cjs");

// Step 1: Read the content of 'lipsum_1.txt'
fsProblem2
  .readFile("lipsum_1.txt")
  .then((data) => {
    console.log(data);
    // Step 2: Create an uppercase version of the content
    return fsProblem2.uppercaseFile(data);
  })
  .then((name) => {
    // Step 3: Write the uppercase content to a new file and append the filename to 'filenames.txt'
    return fsProblem2.writeName(name);
  })
  .then((name) => {
    // Step 4: Read the content of the newly created uppercase file
    return fsProblem2.readFile(name);
  })
  .then((data) => {
    // Step 5: Create a lowercase version of the content
    return fsProblem2.lowercaseFile(data);
  })
  .then((name) => {
    // Step 6: Write the lowercase content to a new file and append the filename to 'filenames.txt'
    return fsProblem2.writeName(name);
  })
  .then((name) => {
    // Step 7: Read the content of the newly created lowercase file
    return fsProblem2.readFile(name);
  })
  .then((data) => {
    // Step 8: Sort and merge the content of both uppercase and lowercase files
    return fsProblem2.sortFile(data);
  })
  .then((name) => {
    // Step 9: Write the sorted content to a new file and append the filename to 'filenames.txt'
    fsProblem2.writeName(name);
    return fsProblem2.readFile("filenames.txt");
  })
  .then((data) => {
    // Step 10: Delete all files mentioned in 'filenames.txt' and log the result
    console.log(fsProblem2.deleteFiles(data));
  })
  .catch((error) => {
    // Log any errors that occur during the process
    console.error(error);
  });
