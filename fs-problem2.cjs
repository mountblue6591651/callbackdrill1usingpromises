const fs = require("fs");
const { resolve } = require("path");

// Function to read a file asynchronously
function readFile(name) {
  return new Promise((resolve, reject) => {
    fs.readFile(`./${name}`, "utf-8", (error, data) => {
      if (error) {
        // Reject the promise if there is an error reading the file
        reject(error);
      } else {
        // Log success message and resolve the promise with the file data
        console.log("File read successfully");
        resolve(data);
      }
    });
  });
}

// Function to create an uppercase version of a file asynchronously
function uppercaseFile(data) {
  return new Promise((resolve, reject) => {
    fs.writeFile("./upperCaseFile.txt", data.toUpperCase(), (err) => {
      if (err) {
        // Reject the promise if there is an error writing the uppercase file
        reject(err);
      } else {
        // Log success message and resolve the promise with the filename
        console.log("UpperCase file created");
        resolve("upperCaseFile.txt");
      }
    });
  });
}

// Function to append a name to a file asynchronously
function writeName(name) {
  return new Promise((resolve, reject) => {
    fs.appendFile("./filenames.txt", `${name}\n`, (error) => {
      if (error) {
        // Reject the promise if there is an error appending the name to the file
        reject(error);
      } else {
        // Log success message and resolve the promise with the name
        console.log(`${name} added to filenames.txt`);
        resolve(name);
      }
    });
  });
}

// Function to create a lowercase version of a file asynchronously
function lowercaseFile(data) {
  return new Promise((resolve, reject) => {
    // Split the data into sentences, convert to lowercase, and join with line breaks
    let sentences = data
      .toLowerCase()
      .split(".")
      .map((sentence) => sentence.trim())
      .join("\n");

    // Create a Promise to append the lowercase sentences to a file
    new Promise((resolve, reject) => {
      fs.appendFile("./lowerCaseSentences.txt", sentences, (error) => {
        if (error) {
          // Reject the promise if there is an error appending to the file
          reject(error);
        } else {
          // Resolve the inner promise with the filename
          resolve("lowerCaseSentences.txt");
        }
      });
    })
      .then((name) => {
        // Log success message and resolve the main promise with the filename
        console.log(`${name} created`);
        resolve(name);
      })
      .catch((error) => {
        // Reject the main promise if there is an error in the inner promise
        reject(error);
      });
  });
}

// Function to sort and merge two files asynchronously
function sortFile(data) {
  return new Promise((resolve, reject) => {
    fs.readFile("./upperCaseFile.txt", "utf-8", (err, data2) => {
      if (err) {
        // Reject the promise if there is an error reading the uppercase file
        reject(err);
      } else {
        // Combine and sort the data from both files
        let totalData = (data + data2).split("\n").sort().join("\n");

        // Write the sorted data to a new file
        fs.writeFile("./sortedFile.txt", totalData, (err) => {
          if (err) {
            // Reject the promise if there is an error writing the sorted file
            reject(err);
          } else {
            // Resolve the promise with the filename
            resolve("sortedFile.txt");
          }
        });
      }
    });
  });
}

// Function to delete multiple files asynchronously
function deleteFiles(data) {
  return new Promise((resolve, reject) => {
    let count = 0;

    // Split the data into filenames and remove trailing whitespace
    let files = data.split("\n").map((file) => file.trim());

    // Create a Promise to delete each file
    new Promise((resolve, reject) => {
      for (let index = 0; index < files.length - 1; index++) {
        fs.rm(`./${files[index]}`, (err) => {
          if (err) {
            // Reject the promise if there is an error deleting a file
            reject(err);
          } else {
            // Increment count and log success message
            count++;
            console.log(`${files[index]} deleted`);
            // Resolve the inner promise
            resolve(true);
          }
        });
      }
    })
      .then(() => {
        // Check if all files are deleted and resolve the main promise
        if (count === files.length) {
          resolve("All files deleted");
        }
      })
      .catch((error) => {
        // Reject the main promise if there is an error in the inner promise
        reject(error);
      });
  });
}

// Export all functions for use in other modules
module.exports = {
  readFile,
  uppercaseFile,
  writeName,
  lowercaseFile,
  sortFile,
  deleteFiles,
};
