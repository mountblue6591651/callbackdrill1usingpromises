const fs = require("fs");
const path = require("path");

// Function to create a folder asynchronously
function createFolder(absolutePathOfRandomDirectory) {
  const promise = new Promise((resolve, reject) => {
    // Create a directory using fs.mkdir
    fs.mkdir(absolutePathOfRandomDirectory, (error) => {
      if (error) {
        // If there's an error, reject the promise with the error
        reject(error);
      } else {
        // If successful, resolve the promise with a success message
        resolve("Directory Created");
      }
    });
  });

  return promise;
}

// Function to create files asynchronously
function createFiles(absolutePathOfRandomDirectory, randomNumberOfFiles) {
  const promise = new Promise((resolve, reject) => {
    let files = 0;

    // Loop to create multiple files
    for (let index = 1; index <= randomNumberOfFiles; index++) {
      let data = JSON.stringify({ key: `${index}` });
      let filePath = path.join(
        absolutePathOfRandomDirectory,
        `file${index}.json`
      );

      // Create a Promise for each file creation using fs.writeFile
      new Promise((resolve, reject) => {
        fs.writeFile(filePath, data, (err) => {
          if (err) {
            // If there's an error, reject the inner promise
            reject(err);
          } else {
            // If successful, increment the files counter and resolve the inner promise
            files++;
            resolve(`File${index} created`);
          }
        });
      })
        .then((msg) => {
          console.log(msg);

          // Check if all files are created before resolving the main promise
          if (files === randomNumberOfFiles) {
            resolve("All Files Created");
          }
        })
        .catch((error) => {
          // If any file creation fails, reject the main promise
          reject(error);
        });
    }
  });

  return promise;
}

// Function to delete files asynchronously
function deleteFiles(absolutePathOfRandomDirectory, randomNumberOfFiles) {
  const promise = new Promise((resolve, reject) => {
    let files = 0;

    // Loop to delete multiple files
    for (let index = 1; index <= randomNumberOfFiles; index++) {
      let filePath = path.join(
        absolutePathOfRandomDirectory,
        `file${index}.json`
      );

      // Create a Promise for each file deletion using fs.rm
      new Promise((resolve, reject) => {
        fs.rm(filePath, (err) => {
          if (err) {
            // If there's an error, reject the inner promise
            reject(err);
          } else {
            // If successful, increment the files counter and resolve the inner promise
            files++;
            resolve(`File${index} deleted`);
          }
        });
      })
        .then((msg) => {
          console.log(msg);

          // Check if all files are deleted before resolving the main promise
          if (files === randomNumberOfFiles) {
            resolve("All Files Deleted");
          }
        })
        .catch((error) => {
          // If any file deletion fails, reject the main promise
          reject(error);
        });
    }
  });

  return promise;
}

module.exports = { createFolder, createFiles, deleteFiles };
